from django.urls import path

from . import views

app_name = 'core'
urlpatterns = [
    path('', views.home, name='home'),
    path('mapa', views.mapa, name='mapa'),
    path('api/mapas', views.api_mapas, name='api_mapas'),
    path('api_interna', views.api_interna, name='api_interna'),
    path('github/<str:username>', views.github, name='github'),
    path('mapa_apagar', views.mapa_apagar, name='mapa_apagar'),
]
