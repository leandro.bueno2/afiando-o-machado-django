"""
O Kubernetes é um sistema de gerir containers.

Ele pode criar e destruir container de acordo com a carga de requisições

O sistema vai ter que emular o kuberbentes levanto em conta que:

Um container pode tratar 2 requisiçoes simultaneamente.
Uma requisição dura 3 tiques de relógico para ser processada.

    >>> k8s = Kubernetes(requisicoes_por_container=2, tiques_por_requisicao=3)
    >>> k8s.tique()
    []
    >>> k8s.tique(requisicoes=1)
    [[3, 0]]
    >>> k8s.tique(requisicoes=2)
    [[2, 3], [3, 0]]
    >>> k8s.tique()
    [[1, 2], [2, 0]]
    >>> k8s.tique(3)
    [[3, 1], [1, 3], [3, 0]]
    >>> k8s.tique(2)
    [[2, 3], [3, 2], [2, 0]]
    >>> k8s.tique()
    [[1, 2], [2, 1], [1, 0]]
    >>> k8s.tique()
    [[0, 1], [1, 0]]
    >>> k8s.tique()
    []
"""
from typing import List


class ContainerCheio(Exception):
    pass


class Container:
    def __init__(self, requisicoes_por_container: int, tiques_por_requisicao: int):
        self.tiques_por_requisicao = tiques_por_requisicao
        self.requisicoes_em_processamento = [0] * requisicoes_por_container  # [0, 0]

    def adicionar_requisicao(self):
        for indice, tiques_restantes in enumerate(self.requisicoes_em_processamento):
            if tiques_restantes == 0:
                self.requisicoes_em_processamento[indice] = self.tiques_por_requisicao
                return
        raise ContainerCheio()

    def processar_requisicoes(self):
        for indice, tiques_restantes in enumerate(self.requisicoes_em_processamento):
            if tiques_restantes > 0:
                self.requisicoes_em_processamento[indice] -= 1

    def tem_requisicoes_ativas(self):
        return sum(self.requisicoes_em_processamento) > 0

    def __repr__(self):
        return repr(self.requisicoes_em_processamento)


class Kubernetes:
    def __init__(self, requisicoes_por_container, tiques_por_requisicao):
        self.tiques_por_requisicao: int = tiques_por_requisicao
        self.requisicoes_por_container: int = requisicoes_por_container
        self._containers: List[Container] = []

    def tique(self, requisicoes: int = 0) -> List[Container]:
        self.processar_requisicoes_de_todos_containers()
        for _ in range(requisicoes):
            requisicao_foi_adicionada = False
            for container in self._containers:
                try:
                    container.adicionar_requisicao()
                except ContainerCheio:
                    continue
                else:
                    requisicao_foi_adicionada = True
                    break

            if not requisicao_foi_adicionada:
                container = Container(self.requisicoes_por_container, self.tiques_por_requisicao)
                self._containers.append(container)
                container.adicionar_requisicao()
        return self._containers

    def processar_requisicoes_de_todos_containers(self):
        for container in self._containers:
            container.processar_requisicoes()
        self._containers = [container for container in self._containers if container.tem_requisicoes_ativas()]
