"""
    >>> from pprint import pprint
    >>> estacionamento = Estaciomento(andares=3, vagas_por_andar=4)
    >>> gol = Carro('gol')
    >>> pprint(estacionamento.estacionar(gol))
    [['gol', 'vazia', 'vazia', 'vazia'],
     ['vazia', 'vazia', 'vazia', 'vazia'],
     ['vazia', 'vazia', 'vazia', 'vazia']]
    >>> pprint(estacionamento.estacionar(Carro('fusca'), Carro('santana'), Carro('premium'), Carro('tempra')))
    [['gol', 'fusca', 'santana', 'premium'],
     ['tempra', 'vazia', 'vazia', 'vazia'],
     ['vazia', 'vazia', 'vazia', 'vazia']]

"""


class Estaciomento:
    def __init__(self, andares=3, vagas_por_andar=4):
        self._andares = [['vazia'] * vagas_por_andar for _ in range(andares)]
        self._quantidade_carros_estacionados = 0

    def estacionar(self, *carros):
        for carro in carros:
            self._estacionar_um_carro(carro)
        return self._andares

    def _estacionar_um_carro(self, carro):
        vagas_por_andar = len(self._andares[0])
        andar, vaga = divmod(self._quantidade_carros_estacionados, vagas_por_andar)
        self._andares[andar][vaga] = carro.marca
        self._quantidade_carros_estacionados += 1


class Carro:
    def __init__(self, marca):
        self.marca = marca
